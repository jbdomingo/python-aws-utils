import boto3
import json
import sys

if len(sys.argv) != 3:
    print("Please specify s3 bucket and policy number")
    sys.exit()

s3_client = boto3.client('s3')

result = s3_client.get_object(Bucket=sys.argv[1], Key=sys.argv[2]) 
parsed = json.loads(result["Body"].read())

if (parsed.get('image')):
    del parsed['image']

new_json = dict()
new_json["Body"] = parsed

print("Policy Number: " + parsed["policy"]["policy#"])
print("Underwriting Class: " + parsed["policy"]["underwritingClass"])