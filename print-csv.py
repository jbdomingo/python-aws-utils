import boto3
import json
import sys
import pandas as pd
import io
import re

if len(sys.argv) != 3:
    print("Please specify s3 bucket and policy number")
    sys.exit()

s3_client = boto3.client('s3')

result = s3_client.get_object(Bucket=sys.argv[1], Key=sys.argv[2]) 
df = pd.read_csv(result["Body"])
print(df[['policy.policy#', 'stpStatus', 'stpFailureReason', 'policy.payment.multiPay.banking.initial.cc.authNumber']])



#for col in df.columns:
#    print(col)

