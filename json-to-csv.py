import os
import pandas as pd
import csv


import json

with open('Hierarchy_Mnemonic_View_Query.json', 'r') as f:
  data = json.load(f)


df = pd.json_normalize(data)


df.to_csv('Hierarchy_Mnemonic_View_Query.csv', index=False, quoting = csv.QUOTE_NONNUMERIC)