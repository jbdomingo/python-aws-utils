import json

import pandas as pd
 
# Opening JSON file
f = open('Hierarchy_Mnemonic_View_Query.json')
 
# returns JSON object as
# a dictionary
data = json.load(f)

df = pd.read_json('Hierarchy_Mnemonic_View_Query_PROD.json')
 
print(len(df.index))
# Closing file
f.close()