import boto3
import json
import sys

if len(sys.argv) != 3:
    print("Please specify s3 bucket and policy number")
    sys.exit()

s3_client = boto3.client('s3')

result = s3_client.get_object(Bucket=sys.argv[1], Key=sys.argv[2]) 
parsed = json.loads(result["Body"].read())

new_json = dict()
new_json["body"] = parsed

with open(sys.argv[2] + '.json', 'w', encoding='utf-8') as file:
    json.dump(new_json, file, ensure_ascii=False, indent=4, sort_keys=False)

