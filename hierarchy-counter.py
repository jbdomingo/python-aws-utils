import json
import pandas as pd
 
df_old = pd.read_json('Hierarchy_Mnemonic_View_Query_20220809.json')
df2_old = df_old.groupby(['PRODUCERID','STATEPROVINCEID'], sort=True, as_index=False).size()
df2_old.columns = ['PRODUCERID', 'STATEPROVINCEID', 'OLD_SIZE']


df_new = pd.read_json('Hierarchy_Mnemonic_View_Query.json')
df2_new = df_new.groupby(['PRODUCERID','STATEPROVINCEID'], sort=True, as_index=False).size()
df2_new.columns = ['PRODUCERID', 'STATEPROVINCEID', 'NEW_SIZE']

outer_merged = pd.merge(df2_old, df2_new, how="outer", on=["PRODUCERID", "STATEPROVINCEID"] )

#outer_merged.to_csv("output.csv", index=False)

new_records =  outer_merged[outer_merged.isnull().any(axis=1)]

print(new_records.to_string(index=False))
#df2.to_json('output20220801.json')